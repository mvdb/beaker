#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
import numpy as np
import os
import sys
import traceback
from multiprocessing import Pool
from time import time
from beaker.tasks import all_tasks
from beaker.utils.filter import get_regexp, REGEXP_NONE
from beaker.utils.io import colorize, read_yaml


def get_run_class(task):
    try:
        RunClass = all_tasks[task]
    except KeyError:
        raise ValueError(f'Unknown task: {task}')
    return RunClass


def merge(d1, d2):
    """Merges nested dictionary d2 into d1 (recursively).

    See also stackoverflow question 29847098.
    """
    for k in d2:
        if k in d1 and isinstance(d1[k], dict) and isinstance(d2[k], dict):
            merge(d1[k], d2[k])
        else:
            d1[k] = d2[k]


class BeakerFailedRunsError(Exception):
    """When one or more runs have failed"""


class RunCustomizer:
    keys = ('structures', 'models', 'tasks')

    def __init__(self, structures=None, models=None, tasks=None,
                 model_kwargs=None, task_kwargs=None):
        for key, pattern in zip(self.keys, (structures, models, tasks)):
            if isinstance(pattern, str) or pattern is None:
                val = get_regexp(pattern, REGEXP_NONE)
            elif isinstance(pattern, list):
                val = get_regexp('|'.join([f'^{item}$' for item in pattern]),
                                 REGEXP_NONE)
            else:
                raise ValueError(f'Pattern "{pattern}" should be either be '
                                 'a list, a string or None')
            setattr(self, key, val)

        self.model_kwargs = dict() if model_kwargs is None else model_kwargs
        self.task_kwargs = dict() if task_kwargs is None else task_kwargs

    def matches(self, struc_name, model_name, task):
        is_match = True
        for key, name in zip(self.keys, (struc_name, model_name, task)):
            regexp = getattr(self, key)
            if regexp.match(name) is None:
                is_match = False
                break
        return is_match

    def get_model_kwargs(self):
        return self.model_kwargs.copy()

    def get_task_kwargs(self):
        return self.task_kwargs.copy()


class RunGenerator:
    def __init__(self, model_files, run_file, struc_filter, task_filter,
                 redo=False, verbose=False):
        self.redo = redo
        self.generate_all_runs(model_files, run_file, struc_filter,
                               task_filter, verbose)

    def generate_all_runs(self, model_files, run_file, *args):
        customizers = []
        if os.path.exists('custom.yaml'):
            print('Applying customizations defined in custom.yaml')
            for customization in read_yaml('custom.yaml', expand_envvars=True):
                customizers.append(RunCustomizer(**customization))

        run_dict = read_yaml(run_file, expand_envvars=True)

        self.runs = dict()
        for model_file in model_files:
            self.add_runs_for_model(model_file, customizers, run_dict, *args)
        return

    def add_runs_for_model(self, model_file, customizers, run_dict,
                           struc_filter, task_filter, verbose):
        ext = '.yaml'
        if (not os.path.exists(model_file)) and (not model_file.endswith(ext)):
            model_file += ext
        default_model_kwargs = read_yaml(model_file, expand_envvars=True)

        model_name, _ = os.path.basename(model_file).rsplit('.', maxsplit=1)

        for struc_name, run_kwargs in run_dict.items():
            if not struc_filter(struc_name):
                continue

            for task in run_kwargs.get('tasks', []):
                if not task_filter(task):
                    continue

                def get_run(task):
                    RunClass = get_run_class(task)

                    model_kwargs = default_model_kwargs.copy()
                    struc_kwargs = run_kwargs.copy()
                    struc_kwargs.pop('tasks', None)
                    task_kwargs = dict()

                    for customizer in customizers:
                        if customizer.matches(struc_name, model_name, task):
                            merge(model_kwargs, customizer.get_model_kwargs())
                            merge(task_kwargs, customizer.get_task_kwargs())

                    return RunClass(model_name, model_kwargs, struc_name,
                                    struc_kwargs, verbose, task,
                                    task_kwargs)

                new_runs = []

                run = get_run(task)
                done = os.path.exists(run.workdir)
                to_be_redone = self.redo
                new_runs.append((run, done))

                while ((not done) or to_be_redone) and run.has_prior_task():
                    task, done = run.check_prior_task()
                    to_be_redone = self.redo and task_filter(task)
                    if (not done) or to_be_redone:
                        run = get_run(task)
                        new_runs.append((run, done))

                for level, (run, done) in enumerate(new_runs[::-1]):
                    if self.need_run(run, done):
                        if level not in self.runs:
                            self.runs[level] = []
                        self.runs[level].append(run)
        return

    def need_run(self, run, done):
        needed = True

        if done and not self.redo:
            needed = False

        if any([run in runs for runs in self.runs.values()]):
            needed = False

        return needed

    def print_run_overview(self):
        nruns = {key: len(val) for key, val in self.runs.items()}
        print(f'Total number of runs: {sum(nruns.values())}')

        for level in sorted(self.runs):
            if nruns[level] > 0:
                print(f'\nNumber of level-{level} runs: {nruns[level]}')
                print('Generated runs (in order of execution):')

                fmt = '{0:<32} {1:<32} {2}'
                print(fmt.format('Model', 'Structure', 'Task'))
                print(fmt.format('='*32, '='*32, '='*12))

                for run in self.runs[level]:
                    print(fmt.format(run.model_name, run.struc_name, run.task))
        return

    def run(self, processes):
        all_good = True
        OK = colorize('OK', 'green')
        failed = colorize('failed', 'red')

        for level in sorted(self.runs):
            procstr = 'process' if processes == 1 else 'processes'
            msg = f'\nStarting level-{level} runs using {processes} {procstr}'
            print(msg, flush=True)

            pool = Pool(processes=processes)
            mapresult = pool.map_async(run_wrapper, self.runs[level],
                                       chunksize=1)
            pool.close()
            pool.join()
            results = mapresult.get()

            for target in [True, False]:
                for success, message in results:
                    if success is target:
                        print(message, flush=True)

                    if not success:
                        all_good = False

            if all_good:
                print(f'Done with level-{level} runs (all {OK})')

            if not all_good:
                break

        if all_good:
            print(f'\nDone (all {OK})')
        else:
            raise BeakerFailedRunsError(f'\nOne or more runs {failed}')
        return


def run_wrapper(run):
    run.initialize_atoms()
    run.create_workdir()

    stdout = os.path.join(run.workdir, 'beaker.out')
    sys.stdout = open(stdout, 'w')
    stderr = os.path.join(run.workdir, 'beaker.err')
    sys.stderr = open(stderr, 'w')

    cwd = os.getcwd()
    os.chdir(run.workdir)
    message = f'Run {run.workdir} :: '
    success = False
    t_start = time()

    try:
        calc_args = run.get_calc_args()
        calc_kwargs = run.get_calc_kwargs()

        run.run(run.atoms, calc_args, calc_kwargs)

        message += colorize('OK', 'green')
        success = True
    except Exception:
        traceback.print_exc()
        sys.stderr.flush()
        message += colorize('FAIL', 'red')

    t_end = time()
    t_delta = int(np.ceil(t_end - t_start))
    message += f' ({t_delta} sec)'

    sys.stdout.close()
    sys.stdout = sys.__stdout__
    sys.stderr.close()
    sys.stderr = sys.__stderr__

    os.chdir(cwd)
    return (success, message)
