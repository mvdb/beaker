#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
import os
from ase.io import read


class RunBase:
    struc_dependency = None

    def __init__(self, model_name, model_kwargs,
                 struc_name, struc_kwargs, verbose,
                 task, task_kwargs):
        self.model_name = model_name
        self.model_kwargs = model_kwargs
        self.struc_name = struc_name
        self.struc_kwargs = struc_kwargs
        self.verbose = verbose
        self.task = task
        self.task_kwargs = task_kwargs
        self.workdir = self.get_workdir()

    def __eq__(self, other):
        return self.workdir == other.workdir

    def apply_custom_task_kwargs(self):
        for key, custom_kwargs in self.task_kwargs.items():
            kwargs = getattr(self, key)
            kwargs.update(custom_kwargs)
        return

    def get_calc_args(self):
        return (self.model_name, self.model_kwargs, self.struc_kwargs)

    def get_calc_kwargs(self):
        return dict(verbose=self.verbose)

    def get_workdir(self):
        parts = [self.model_name, self.struc_name, self.task]
        workdir = os.path.join(*parts)
        return workdir

    def create_workdir(self):
        if not os.path.exists(self.workdir):
            os.makedirs(self.workdir, exist_ok=True)
        return

    def get_abstract_prior_run(self):
        assert self.struc_dependency is not None
        prior_task, _ = self.struc_dependency
        prior_run = RunBase(self.model_name, self.model_kwargs,
                            self.struc_name, self.struc_kwargs,
                            self.verbose, prior_task, self.task_kwargs)
        return prior_run

    def get_prior_run_workdir(self):
        prior_run_workdir = self.get_abstract_prior_run().get_workdir()
        return prior_run_workdir

    def get_starting_structure_filename(self):
        if self.struc_dependency is None:
            filename = self.struc_kwargs.get('filename', None)
            if filename is None:
                filename = f'start_{self.struc_name}.traj'
        else:
            _, prior_run_fname = self.struc_dependency
            prior_run_workdir = self.get_prior_run_workdir()
            filename = os.path.join(prior_run_workdir, prior_run_fname)
        return filename

    def check_prior_task(self):
        prior_task, _ = self.struc_dependency
        filename = self.get_starting_structure_filename()
        done = os.path.exists(filename)
        return prior_task, done

    def has_prior_task(self):
        return self.struc_dependency is not None

    def initialize_atoms(self):
        filename = self.get_starting_structure_filename()

        if not os.path.exists(filename):
            msg = f'The "{self.task}" task for structure "{self.struc_name}" '
            msg += f'needs a "{filename}" file as input, '
            msg += 'but this file does not exist'
            raise FileNotFoundError(msg)

        self.atoms = read(filename)
        self.atoms.info.update(struc_name=self.struc_name,
                               struc_kwargs=self.struc_kwargs,
                               struc_dependency=self.struc_dependency)

        if not self.struc_kwargs.get('spinpol', False):
            # Some ASE calculators (e.g. Espresso) will not respect
            # spinpol=False unless the initial magmoms are zero
            self.atoms.set_initial_magnetic_moments([0.] * len(self.atoms))

        return
