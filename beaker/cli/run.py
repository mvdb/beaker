#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from beaker.runs.generator import BeakerFailedRunsError, RunGenerator
from beaker.utils.filter import Filter


def parse_arguments():
    description = """
    Run selected atomic simulations.

    Example:

    beaker-run -m tibi_dzp.yaml -r myruns.yaml --tasks=relax
    """
    parser = ArgumentParser(description=description,
                            formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('--dry-run', '-d', action='store_true',
                        help='Exit after printing the run overview, '
                             'without executing them.')
    parser.add_argument('--exclude', '-x', type=str,
                        help='Regular expression for excluding specific '
                             'structures. By default none get excluded.')
    parser.add_argument('--include', '-i', type=str,
                        help='Regular expression for only selecting specific '
                             'structures. By default all are included.')
    parser.add_argument('--models', '-m', type=str, required=True,
                        help='Comma-separated paths to YAML files which '
                             'define the model names and the settings for '
                             'corresponding calculators. Including the '
                             '".yaml" extension for the files is optional.')
    parser.add_argument('--processes', '-p', type=int, default=1,
                        help='Number of processes to use for multiprocessing '
                             '(default: %(default)s).')
    parser.add_argument('--redo', action='store_true',
                        help='With this flag, runs that have already been '
                             'performed will be re-run instead of being '
                             'skipped.')
    parser.add_argument('--runfile', '-r', type=str, default='runs.yaml',
                        help='Path to a YAML file defining the different '
                        '"runs", where each run is a structure+task '
                        'combination. Default: "%(default)s".')
    parser.add_argument('--tasks', '-t', type=str,
                        help='Regular expression for only selecting specific '
                             'tasks. By default all requested ones are '
                             'performed.')
    parser.add_argument('--verbose', action='store_true',
                        help='Triggers verbose calculator output.')
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()

    model_files = args.models.split(',')
    struc_filter = Filter(include=args.include, exclude=args.exclude)
    task_filter = Filter(include=args.tasks, exclude=None)

    generator = RunGenerator(model_files, args.runfile, struc_filter,
                             task_filter, redo=args.redo,
                             verbose=args.verbose)
    generator.print_run_overview()

    exitcode = 0

    if not args.dry_run:
        try:
            generator.run(args.processes)
        except BeakerFailedRunsError as e:
            # Only print error message, no traceback
            print(e)
            exitcode = 1

    return exitcode


if __name__ == '__main__':
    main()
