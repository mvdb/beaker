#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from beaker.analyse.collect import Collector
from beaker.analyse.compare import Comparator
from beaker.utils.filter import Filter


def parse_arguments():
    description = """
    Compare properties extracted from selected atomic simulations.

    Example:

    beaker-compare results_tibi_dzp.json --ref=results_gpaw_pw.json \\
                   --include=.*coord
    """
    parser = ArgumentParser(description=description,
                            formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('results', nargs='*', help='Paths to JSON files '
                        'with results to be compared to the given reference.')
    parser.add_argument('--exclude', '-x', type=str,
                        help='Regular expression for excluding specific '
                             'properties from the results files. '
                             'By default none get excluded.')
    parser.add_argument('--include', '-i', type=str,
                        help='Regular expression for only selecting specific '
                             'properties from the results files. '
                             'By default all are included.')
    parser.add_argument('--ref', '-r', type=str, required=True,
                        help='Path to a JSON file with the reference results.')
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()

    prop_filter = Filter(include=args.include, exclude=args.exclude)

    reference = Collector.from_resultfile(args.ref, prop_filter)

    results = []
    for resultfile in args.results:
        result = Collector.from_resultfile(resultfile, prop_filter)
        results.append(result)

    comparator = Comparator(reference, results)
    comparator.compare()
    comparator.print_summary()
    return


if __name__ == '__main__':
    main()
