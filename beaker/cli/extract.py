#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from beaker.analyse.collect import Collector
from beaker.utils.filter import Filter


def parse_arguments():
    description = """
    Extract calculated properties from selected atomic simulations.

    Example:

    beaker-extract --models=tibi_dzp --properties=properties.yaml
    """
    parser = ArgumentParser(description=description,
                            formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('--exclude', '-x', type=str,
                        help='Regular expression for excluding specific '
                             'properties from the property file. '
                             'By default none get excluded.')
    parser.add_argument('--include', '-i', type=str,
                        help='Regular expression for only selecting specific '
                             'properties from the property file. '
                             'By default all are included.')
    parser.add_argument('--models', '-m', type=str, required=True,
                        help='Comma-separated paths to the model directories.')
    parser.add_argument('--output', '-o', type=str,
                        help='Comma-separated list of JSON files (one for '
                             'each model) where the properties will be added. '
                             'Default: "results_{model_name}.json" for each '
                             'model.')
    parser.add_argument('--properties', '-p', type=str,
                        default='properties.yaml',
                        help='Path to a YAML property file defining the '
                             'properties to extract. Default: "%(default)s".')
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()

    model_dirs = args.models.split(',')
    prop_filter = Filter(include=args.include, exclude=args.exclude)

    if args.output is None:
        output_files = [None] * len(model_dirs)
    else:
        output_files = args.output.split(',')
        assert len(output_files) == len(model_dirs), \
               'The number of output files does not match the number of models'

    for model_dir, output_file in zip(model_dirs, output_files):
        collector = Collector.from_yaml(model_dir, args.properties,
                                        prop_filter)
        collector.extract()
        collector.write(output_file=output_file)
    return


if __name__ == '__main__':
    main()
