#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
from random import sample
from string import ascii_letters, digits


def generate_random_string(length):
    """Returns a random alphanumeric string of the given length."""
    population = ascii_letters + digits
    choice = sample(population, length)
    string = ''.join(choice)
    return string
