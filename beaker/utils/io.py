#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
import json
import os
import yaml
from ase.io.jsonio import MyEncoder, read_json as read_json_ase
from ase.utils import writer
from pyaml_env import parse_config


def colorize(string, color):
    if color == 'green':
        template = '\033[92m{0}\033[0m'
    elif color == 'red':
        template = '\033[91m{0}\033[0m'
    else:
        raise NotImplementedError(color)

    colorstring = template.format(string)
    return colorstring


def prepend_initial_working_directory(dir):
    if dir.startswith('/'):
        return dir
    else:
        return os.path.join('..', '..', '..', dir)


def read_json(filename, use_ase=False):
    with open(filename, 'r') as f:
        if use_ase:
            content = read_json_ase(f)
        else:
            content = json.load(f)
    return content


def read_yaml(filename, expand_envvars=False):
    if expand_envvars:
        content = parse_config(filename)
    else:
        with open(filename, 'r') as f:
            content = yaml.safe_load(f)
    return content


myencode = MyEncoder(sort_keys=True, indent=4).encode


@writer
def write_json_ase(fd, obj):
    fd.write(myencode(obj))


def write_json(content, filename, use_ase=False):
    with open(filename, 'w') as f:
        if use_ase:
            write_json_ase(f, content)
        else:
            json.dump(content, f)
    return
