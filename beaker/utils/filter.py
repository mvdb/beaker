#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
import re


REGEXP_ALL = '.*'
REGEXP_NONE = '$^'


def get_regexp(pattern, default):
    return re.compile(default) if pattern is None else re.compile(pattern)


class Filter:
    def __init__(self, include=None, exclude=None):
        self.regexp_include = get_regexp(include, REGEXP_ALL)
        self.regexp_exclude = get_regexp(exclude, REGEXP_NONE)

    def __call__(self, string):
        included = re.match(self.regexp_include, string) is not None
        excluded = re.match(self.regexp_exclude, string) is not None
        is_match = included and not excluded
        return is_match
