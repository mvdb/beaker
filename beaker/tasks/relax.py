#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
from ase.calculators.socketio import SocketIOCalculator
from ase.constraints import UnitCellFilter
from ase.io import write
from ase.optimize import BFGS
from beaker.calculators.calculator import get_calculator
from beaker.runs.base import RunBase
from beaker.tasks.eos import EOS


def assert_filelog(logfile):
    assert isinstance(logfile, str) and logfile != '-', \
           'Only logging to file is supported'
    return


class Relax(RunBase):
    struc_dependency = None

    def __init__(self, *args):
        super().__init__(*args)

        self.opt_initkwargs = dict(logfile='opt.log', trajectory='opt.traj')
        self.opt_runkwargs = dict(fmax=2e-2, steps=500)
        self.apply_custom_task_kwargs()

    def run(self, atoms, calc_args, calc_kwargs, vc=False):
        calc = get_calculator(atoms, *calc_args, **calc_kwargs)

        if vc and 'stress' not in calc.implemented_properties:
            # Explicit check needed because it's customary for applications
            # to just return zeros in the i-Pi stress tensor buffer
            # when no (analytical) stress tensor is available
            msg = 'This calculator does not provide a stress tensor'
            raise NotImplementedError(msg)

        socket_kwargs = dict(unixsocket=calc.unixsocket)

        with SocketIOCalculator(calc, **socket_kwargs) as socketcalc:
            atoms.calc = socketcalc

            logfile = self.opt_initkwargs['logfile']
            assert_filelog(logfile)

            with open(logfile, 'w') as fhandle:
                self.opt_initkwargs.update(logfile=fhandle)

                if vc:
                    ucf = UnitCellFilter(atoms)
                    opt = BFGS(ucf, **self.opt_initkwargs)
                else:
                    opt = BFGS(atoms, **self.opt_initkwargs)

                opt.run(**self.opt_runkwargs)

            if not opt.converged():
                raise RuntimeError('Optimization did not converge')

            write('final_atoms.traj', atoms)
            e = atoms.get_potential_energy(force_consistent=True)
            print('final_energy [eV]:', e)

            if vc:
                v0 = atoms.get_volume()
                print('final_volume [Ang^3]:', v0)
                print('final_cellpar:', atoms.cell.cellpar())

            print('done')
            atoms.calc.server.protocol.end()

        return


class RelaxEOS(EOS):
    struc_dependency = None

    def run(self, *args, **kwargs):
        EOS.run(self, *args, calculate_at_equilibrium=True, **kwargs)
        return


class RelaxVC(Relax):
    def run(self, *args, **kwargs):
        Relax.run(self, *args, vc=True, **kwargs)
        return
