#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
import numpy as np
from ase.calculators.socketio import SocketIOCalculator
from ase.units import kB
from ase.vibrations import Vibrations
from beaker.calculators.calculator import get_calculator
from beaker.runs.base import RunBase


class Vib(RunBase):
    struc_dependency = ('relax', 'final_atoms.traj')

    def __init__(self, *args):
        super().__init__(*args)

        self.vib_initkwargs = dict(delta=0.01, nfree=2)
        self.vib_analysiskwargs = dict(method='standard', direction='central')
        self.vib_writemodekwargs = dict(kT=500*kB, nimages=30)
        self.apply_custom_task_kwargs()

    def run(self, atoms, calc_args, calc_kwargs):
        calc = get_calculator(atoms, *calc_args, **calc_kwargs)

        socket_kwargs = dict(unixsocket=calc.unixsocket)

        with SocketIOCalculator(calc, **socket_kwargs) as socketcalc:
            atoms.calc = socketcalc

            e = atoms.get_potential_energy(force_consistent=True)
            print('initial_energy [eV]:', e)

            vib = Vibrations(atoms, **self.vib_initkwargs)
            vib.clean()
            vib.run()

            with open('final_summary.txt', 'w') as fhandle:
                vib.summary(log=fhandle, **self.vib_analysiskwargs)

            vib.write_mode(**self.vib_writemodekwargs)

            natoms = len(atoms)
            nmodes = 3 * natoms
            modes = np.zeros((nmodes, natoms, 3))

            # frequencies in eV:
            energies = vib.get_energies(**self.vib_analysiskwargs)
            # frequencies in cm^-1
            frequencies = vib.get_frequencies(**self.vib_analysiskwargs)

            eigenvalues = np.zeros((nmodes, 2), dtype=complex)

            for i in range(nmodes):
                modes[i, :, :] = vib.get_mode(i)
                eigenvalues[i, :] = [energies[i], frequencies[i]]

            np.save('final_eigenmodes.npy', modes)
            np.savetxt('final_eigenvalues.txt', eigenvalues)

            start = max(200, np.min(frequencies.real) - 200)
            end = min(1e5, np.max(frequencies.real) + 200)
            vib.write_dos(out='final_dos.txt', start=start, end=end,
                          **self.vib_analysiskwargs)

            print('done')
            atoms.calc.server.protocol.end()

        return
