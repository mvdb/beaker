#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
import numpy as np
from ase.calculators.socketio import SocketIOCalculator
from ase.dimer import DimerControl, MinModeAtoms, MinModeTranslate
from ase.io import Trajectory, write
from beaker.calculators.calculator import get_calculator
from beaker.runs.base import RunBase
from beaker.tasks.relax import assert_filelog
from beaker.utils.io import prepend_initial_working_directory


class Dimer(RunBase):
    struc_dependency = None

    def __init__(self, *args):
        super().__init__(*args)

        self.dimctrl_initkwargs = dict(
            dimer_separation=5e-3,
            displacement_method='vector',
            eigenmode_logfile='eigenmode.log',
            initial_eigenmode_method='displacement',
            logfile='dimer.log',
            max_num_rot=6,
            maximum_translation=0.05,
            trial_angle=np.pi/10,
        )
        self.dimmode_initkwargs = dict(
            random_seed=123456,
            eigenmodes=None,
        )
        self.dimtrans_initkwargs = dict(
            logfile='opt.log',
            trajectory='opt.traj',
        )
        self.dimopt_runkwargs = dict(
            fmax=2e-2,
            steps=500,
        )
        self.apply_custom_task_kwargs()

    def run(self, atoms, calc_args, calc_kwargs):
        try:
            dimer_info = atoms.info['struc_kwargs']['dimer_info']
            startmode_filename = prepend_initial_working_directory(
                                    dimer_info['startmode_filename'])
        except KeyError:
            msg = 'The "dimer_info" attribute in the runfile for ' + \
                  f'"{atoms.info["struc_name"]}" is missing or incomplete'
            raise ValueError(msg)

        if self.dimmode_initkwargs['eigenmodes'] is None:
            startmode = np.loadtxt(startmode_filename)
            self.dimmode_initkwargs.update(eigenmodes=[startmode])

        calc = get_calculator(atoms, *calc_args, **calc_kwargs)

        socket_kwargs = dict(unixsocket=calc.unixsocket)

        with SocketIOCalculator(calc, **socket_kwargs) as socketcalc:
            atoms.calc = socketcalc

            ctrl_logfile = self.dimctrl_initkwargs['logfile']
            assert_filelog(ctrl_logfile)

            eigenmode_logfile = self.dimctrl_initkwargs['eigenmode_logfile']
            assert_filelog(eigenmode_logfile)

            trans_logfile = self.dimtrans_initkwargs['logfile']
            assert_filelog(trans_logfile)

            with open(ctrl_logfile, 'w') as ctrl_fhandle, \
                 open(eigenmode_logfile, 'w') as eigenmode_fhandle, \
                 open(trans_logfile, 'w') as trans_fhandle:

                self.dimctrl_initkwargs.update(
                        logfile=ctrl_fhandle,
                        eigenmode_logfile=eigenmode_fhandle,
                )
                self.dimtrans_initkwargs.update(logfile=trans_fhandle)

                with DimerControl(**self.dimctrl_initkwargs) as d_control:
                    d_atoms = MinModeAtoms(atoms, d_control,
                                           **self.dimmode_initkwargs)
                    natoms = len(atoms)

                    # Displace the atoms
                    disp = np.zeros((natoms, 3))
                    d_atoms.displace(displacement_vector=disp,
                                     mask=[True]*natoms)

                    # Converge to a saddle point
                    with MinModeTranslate(d_atoms,
                                          **self.dimtrans_initkwargs) as opt:
                        opt.run(**self.dimopt_runkwargs)

                    if not opt.converged():
                        raise RuntimeError('Optimization did not converge')

                    print('final_curvature:', d_atoms.get_curvature())

                    mode = d_atoms.get_eigenmode()
                    np.savetxt('final_mode.txt', mode)

            write('final_atoms.traj', atoms)
            e = atoms.get_potential_energy(force_consistent=True)
            print('final_energy [eV]:', e)

            with Trajectory('final_mode.traj', 'w') as traj:
                for i in range(-5, 6):
                    a = atoms.copy()
                    a.positions += 0.05 * i * mode
                    traj.write(a)

            print('done')
            atoms.calc.server.protocol.end()

        return
