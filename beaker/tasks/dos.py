#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
import matplotlib.pyplot as plt
import numpy as np
from ase.dft.dos import DOS as DOS_ase
from beaker.calculators.calculator import get_calculator
from beaker.runs.base import RunBase


class DOS(RunBase):
    struc_dependency = ('relax', 'final_atoms.traj')

    def __init__(self, *args):
        super().__init__(*args)

        self.dos_initkwargs = dict(width=0.1, window=(-20., 10.))
        self.apply_custom_task_kwargs()

    def run(self, atoms, calc_args, calc_kwargs):
        try:
            bands_info = atoms.info['struc_kwargs']['dos_info']
            is_metallic = bands_info['is_metallic']
        except KeyError:
            msg = 'The "dos_info" attribute in the runfile for ' + \
                  f'"{atoms.info["struc_name"]}" is missing or incomplete'
            raise ValueError(msg)

        calc = get_calculator(atoms, *calc_args, use_socket_io=False,
                              **calc_kwargs)

        calc.setup_dos_run()
        atoms.calc = calc

        e0 = atoms.get_potential_energy()
        print('initial_energy [eV]:', e0)

        m = calc.get_magnetization()
        print('magnetization [-|e|]:', m)

        e_fermi, e_vbm, e_cbm = DOS.get_fermi_vbm_cbm(calc, is_metallic)
        DOS.plot_dos(calc, e_fermi, e_vbm, **self.dos_initkwargs)

        print('done')
        return

    @staticmethod
    def plot_dos(calc, e_fermi, e_vbm, **dos_initkwargs):
        dos = DOS_ase(calc, **dos_initkwargs)
        energies = dos.get_energies() + e_fermi - e_vbm

        nspin = calc.get_number_of_spins()
        densities = []
        labels = []

        if nspin == 1:
            densities.append(dos.get_dos())
            labels.append('total')
        else:
            for ispin in range(nspin):
                densities.append(dos.get_dos(spin=ispin))
                labels.append(f'spin_{ispin}')

        for density, label in zip(densities, labels):
            plt.plot(energies, density, label=label)

        header = '    '.join([f'DOS ({label})' for label in labels])
        np.savetxt('final_dos.txt', np.transpose([energies] + densities),
                   header=f'# Energy    {header}')

        plt.xlabel('Energy [eV]')
        plt.ylabel('DOS')
        plt.legend()
        plt.grid()
        plt.savefig('final_dos.png')
        plt.clf()
        return

    @staticmethod
    def get_fermi_vbm_cbm(calc, is_metallic, tol=1e-2):
        e_fermi = calc.get_fermi_level()
        print('fermi_energy [eV]:', e_fermi)

        if is_metallic:
            e_vbm = e_fermi
            e_cbm = e_fermi
        else:
            nspins = calc.get_number_of_spins()
            nkpts = len(calc.get_k_point_weights())

            e_vbm = -np.inf
            e_cbm = np.inf
            has_fractional_occupations = False

            for ispin in range(nspins):
                for ikpt in range(nkpts):
                    iks = dict(kpt=ikpt, spin=ispin)
                    occupations = calc.get_occupation_numbers(**iks)
                    energies = calc.get_eigenvalues(**iks)

                    for energy, occupation in zip(energies, occupations):
                        occ_abs = abs(occupation)
                        if nspins == 1:
                            occ_abs *= 0.5

                        if 1 - tol > occ_abs > tol:
                            has_fractional_occupations = True

                        if energy > e_vbm and occ_abs > tol:
                            e_vbm = energy
                        elif energy < e_cbm and occ_abs < tol:
                            e_cbm = energy

            if abs(e_vbm - e_fermi) < tol or has_fractional_occupations:
                e_cbm = e_vbm

        print('valence_band_maximum [eV]:', e_vbm)
        print('conduction_band_minimum [eV]:', e_cbm)
        print('band_gap [eV]:', e_cbm - e_vbm)
        return (e_fermi, e_vbm, e_cbm)
