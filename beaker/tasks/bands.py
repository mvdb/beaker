#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
import matplotlib.pyplot as plt
import numpy as np
from ase.spectrum.band_structure import BandStructure
from beaker.calculators.calculator import get_calculator
from beaker.runs.base import RunBase
from beaker.tasks.dos import DOS


class Bands(RunBase):
    struc_dependency = ('relax_vc', 'final_atoms.traj')

    def __init__(self, *args):
        super().__init__(*args)

        self.dos_initkwargs = dict()
        self.apply_custom_task_kwargs()

    def run(self, atoms, calc_args, calc_kwargs):
        try:
            bands_info = atoms.info['struc_kwargs']['bands_info']
            bandpath = bands_info['bandpath']
            npoints = bands_info['npoints']
            is_metallic = bands_info['is_metallic']
        except KeyError:
            msg = 'The "bands_info" attribute in the runfile for ' + \
                  f'"{atoms.info["struc_name"]}" is missing or incomplete'
            raise ValueError(msg)

        calc = get_calculator(atoms, *calc_args, use_socket_io=False,
                              **calc_kwargs)

        # First the (self-consistent) preparatory run
        calc.setup_dos_run()
        calc.setup_restart_run()
        atoms.calc = calc

        e0 = atoms.get_potential_energy()
        print('initial_energy [eV]:', e0)

        m = calc.get_magnetization()
        print('magnetization [-|e|]:', m)

        e_fermi, e_vbm, e_cbm = DOS.get_fermi_vbm_cbm(calc, is_metallic)
        DOS.plot_dos(calc, e_fermi, e_vbm, **self.dos_initkwargs)

        # Now the (non-self-consistent) band structure run
        calc.setup_bandstructure_run(atoms, bandpath, npoints)

        _ = atoms.get_potential_energy()
        Bands.plot_bands(atoms, calc, e_vbm, bandpath, npoints)

        print('done')
        return

    @staticmethod
    def plot_bands(atoms, calc, e_vbm, bandpath, npoints):
        nspins = calc.get_number_of_spins()
        nkpts = len(calc.get_k_point_weights())
        nbands = np.size(calc.get_eigenvalues(kpt=0, spin=0))

        energies = np.zeros((nspins, nkpts, nbands))
        for ispin in range(nspins):
            for ikpt in range(nkpts):
                energies[ispin, ikpt, :] = \
                    calc.get_eigenvalues(kpt=ikpt, spin=ispin)

        path = atoms.cell.bandpath(bandpath, npoints)
        bs = BandStructure(path, energies=energies, reference=e_vbm)
        bs = bs.subtract_reference()
        np.save('final_energies.npy', bs.energies - e_vbm)
        bs.write('final_bs.json')
        bs.path.write('final_path.json')
        bs.plot(filename='final_bands.png')
        plt.clf()
        return
