#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
from beaker.tasks.bands import Bands
from beaker.tasks.dimer import Dimer
from beaker.tasks.dos import DOS
from beaker.tasks.eos import EOS
from beaker.tasks.relax import Relax, RelaxEOS, RelaxVC
from beaker.tasks.vib import Vib


all_tasks = {
    'bands': Bands,
    'dimer': Dimer,
    'dos': DOS,
    'eos': EOS,
    'relax': Relax,
    'relax_eos': RelaxEOS,
    'relax_vc': RelaxVC,
    'vib': Vib,
}
