#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
import matplotlib.pyplot as plt
import numpy as np
from ase.calculators.socketio import SocketIOCalculator
from ase.eos import EquationOfState
from ase.io import write
from ase.units import kJ
from beaker.calculators.calculator import get_calculator
from beaker.runs.base import RunBase


class EOS(RunBase):
    struc_dependency = ('relax_vc', 'final_atoms.traj')

    def __init__(self, *args):
        super().__init__(*args)

        self.eos_initkwargs = dict(eos='sj')
        self.eos_runkwargs = dict(cell_fractions=[0.95, 0.975, 1, 1.025, 1.05])
        self.apply_custom_task_kwargs()

    def run(self, atoms, calc_args, calc_kwargs,
            calculate_at_equilibrium=False):
        calc = get_calculator(atoms, *calc_args, **calc_kwargs)

        socket_kwargs = dict(unixsocket=calc.unixsocket)

        with SocketIOCalculator(calc, **socket_kwargs) as socketcalc:
            atoms.calc = socketcalc

            e0 = atoms.get_potential_energy(force_consistent=True)
            print('initial_energy [eV]:', e0)
            v0 = atoms.get_volume()
            print('initial_volume [Ang^3]:', v0)

            cell = atoms.get_cell()
            energies, volumes = [], []

            for x in self.eos_runkwargs['cell_fractions']:
                if x == 1:
                    e, v = e0, v0
                else:
                    print('applying scaling factor [-]:', x, flush=True)
                    e, v = EOS.get_energy_volume(atoms, x, cell)
                    print('  corresponding energy [eV]:', e)
                energies.append(e)
                volumes.append(v)

            np.savetxt('final_energies.txt', energies)
            np.savetxt('final_volumes.txt', volumes)

            eos = EquationOfState(volumes, energies, **self.eos_initkwargs)
            V, E, B = eos.fit()
            print('final_V [Ang^3]:', V)
            print('final_E [eV]:', E)
            print('final_B [GPa]:', B / kJ * 1e24)
            eos.plot('final_eos.png')
            plt.clf()

            xopt = np.cbrt(V / v0)
            print('equilibrium scaling factor [-]:', xopt)

            atoms.set_cell(cell * xopt, scale_atoms=True)
            if calculate_at_equilibrium:
                e = atoms.get_potential_energy(force_consistent=True)
                print('final_energy [eV]:', e)
            write('final_atoms.traj', atoms)

            v = atoms.get_volume()
            print('final_volume [Ang^3]:', v)
            print('final_cellpar:', atoms.cell.cellpar())

            print('done')
            atoms.calc.server.protocol.end()

        return

    @staticmethod
    def get_energy_volume(atoms, x, cell):
        atoms.set_cell(cell * x, scale_atoms=True)
        e = atoms.get_potential_energy(force_consistent=True)
        v = atoms.get_volume()
        return (e, v)
