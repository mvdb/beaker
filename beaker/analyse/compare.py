#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
import numpy as np
import warnings
from beaker.analyse.properties import Property


class Comparator:
    def __init__(self, reference, results):
        self.reference = reference
        self.results = results
        self.diffs = None
        self.values = None

    def compare(self):
        warn_unavailable = 'unavailable "{0}" entry for model "{1}"'
        err_duplicate = 'duplicate label "{0}"'

        model_ref = self.reference.get_model_name()
        model_names = [result.get_model_name() for result in self.results]

        self.diffs = dict()
        self.values = dict()

        for tag in self.reference.get_unique_tags():
            self.diffs[tag] = {model: dict() for model in model_names}
            self.values[tag] = dict()

        for prop_ref in self.reference.get_properties(sort=True):
            label = prop_ref.get_label()
            value = prop_ref.get_value()
            _, tag = Property.get_label_and_tag_from_key(label)

            assert label not in self.values[tag], \
                   err_duplicate.format((tag, label))

            if value == Property.UNAVAILABLE:
                warnings.warn(warn_unavailable.format(label, model_ref))
                continue

            self.values[tag][label] = {model_ref: value}

            for result in self.results:
                model = result.get_model_name()
                assert label not in self.diffs[tag][model], \
                       err_duplicate.format(label)

                try:
                    prop_model = result[label]
                except KeyError:
                    warnings.warn(warn_unavailable.format(label, model))
                    prop_model = None

                if prop_model is not None:
                    value = prop_model.get_value()
                    if value == Property.UNAVAILABLE:
                        warnings.warn(warn_unavailable.format(label, model))
                        diff = Property.UNAVAILABLE
                    else:
                        diff = prop_model.compare(prop_ref)

                    self.values[tag][label][model] = value
                    self.diffs[tag][model][label] = diff
        return

    def print_summary(self, nworst_max=5):
        def get_table_line(*args):
            template = '{:50s}'
            separator = ' | '

            for arg in args[1:]:
                if isinstance(arg, str):
                    fmt = '{:>7s}'
                elif isinstance(arg, float):
                    fmt = '{:7.3f}'
                elif isinstance(arg, int):
                    fmt = '{:7d}'
                else:
                    raise NotImplementedError(type(arg))
                template += separator + fmt

            line = template.format(*args)
            return line

        model_ref = self.reference.get_model_name()
        model_names = [result.get_model_name() for result in self.results]

        print('# Legend')
        print('# ------')
        print()

        aliases = {model_ref: 'Ref.'}
        print(f'# {aliases[model_ref]}: {model_ref}')
        for i, model in enumerate(model_names):
            aliases[model] = f'Mod.{i+1}'
            print(f'# {aliases[model]}: {model}')
        print()

        columns_all = ['# Label', aliases[model_ref]]
        columns_all += [aliases[model] for model in model_names]
        columns_noref = columns_all.copy()
        columns_noref.pop(1)

        print('# Overview')
        print('# --------')
        print()
        tags = self.reference.get_unique_tags()

        for tag in tags:
            one = next(iter(self.values[tag].values()))[model_ref]
            is_scalar = isinstance(one, int) or isinstance(one, float)

            if is_scalar:
                print(f'# {tag} property (values)')
                line = get_table_line(*columns_all)
                print(line)

                for label, value_dict in self.values[tag].items():
                    items = [value_dict[model_ref]]
                    items += [
                        self.values[tag][label][model]
                        for model in model_names
                    ]
                    line = get_table_line(label, *items)
                    print(line)

                print()

            print(f'# {tag} property (deviations)')
            if is_scalar:
                line = get_table_line(*columns_all)
            else:
                line = get_table_line(*columns_noref)
            print(line)

            for label, value_dict in self.values[tag].items():
                items = [value_dict[model_ref]] if is_scalar else []
                items += [
                    self.diffs[tag][model][label]
                    for model in model_names
                ]
                line = get_table_line(label, *items)
                print(line)

            print()

        print('# Statistics')
        print('# ----------')
        print()

        metrics = ['ntot', 'maxdev', 'msd', 'mad', 'std']

        for tag in tags:
            print(f'# {tag} property')
            columns_metrics = columns_noref.copy()
            columns_metrics[0] = 'Metric'
            line = get_table_line(*columns_metrics)
            print(line)

            scores = {metric: {} for metric in metrics}
            worst = {model: {} for model in model_names}

            for model in model_names:
                labels = []
                diffs = []
                for label in self.diffs[tag][model].keys():
                    diff = self.diffs[tag][model][label]
                    if diff != Property.UNAVAILABLE:
                        labels.append(label)
                        diffs.append(diff)
                diffs = np.array(diffs)

                scores['ntot'][model] = len(diffs)
                scores['maxdev'][model] = np.max(np.abs(diffs))
                scores['msd'][model] = np.average(diffs)
                scores['mad'][model] = np.average(np.abs(diffs))
                scores['std'][model] = np.std(diffs)

                indices = np.argsort(np.abs(diffs))
                nworst = min(len(indices), nworst_max)

                worst[model] = [
                    [labels[i], diffs[i]]
                    for i in indices[-nworst:][::-1]
                ]

            for metric in metrics:
                items = [scores[metric][model] for model in model_names]
                line = get_table_line(metric, *items)
                print(line)

            for model in model_names:
                desc = f'# Largest deviations for {aliases[model]}'
                line = get_table_line(desc, 'diff')
                print(line)

                nworst = min(len(worst[model]), nworst_max)
                for i in range(nworst):
                    line = get_table_line(*worst[model][i])
                    print(line)

            print()
        return
