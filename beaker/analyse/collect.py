#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
import os
from beaker.analyse.properties import (compound_properties, Property,
                                       simple_properties)
from beaker.utils.io import read_json, read_yaml, write_json


class Collector(dict):
    def __init__(self, model_dir, properties):
        self.model_dir = model_dir

        dictionary = {prop.get_label(): prop for prop in properties}
        super().__init__(dictionary)

        def splitter(k):
            label, tag = Property.get_label_and_tag_from_key(k)
            return (tag, label)

        self.sorted_keys = sorted(self.keys(), key=splitter)

    @classmethod
    def from_resultfile(cls, result_file, prop_filter):
        result_dict = read_json(result_file, use_ase=True)

        properties = []
        for key, val in result_dict.items():
            if not prop_filter(key):
                continue

            _, tag = Property.get_label_and_tag_from_key(key)

            if tag in simple_properties:
                Prop = simple_properties[tag]
                prop = Prop()
            elif tag in compound_properties:
                Prop = compound_properties[tag]
                prop = Prop()
            else:
                raise ValueError(f'Unknown property: {tag}')

            prop.set_label(key)
            prop.set_value(val)
            properties.append(prop)

        return cls(result_file, properties)

    @classmethod
    def from_yaml(cls, model_dir, filename, prop_filter):
        prop_dict = read_yaml(filename)
        errmsg = 'YAML parsing error: expected {0}, got {1}'

        properties = []

        for key, val in prop_dict.items():
            if not prop_filter(key):
                continue

            assert isinstance(val, dict), errmsg.format('dict', val)

            _, tag = Property.get_label_and_tag_from_key(key)

            if tag in simple_properties:
                Prop = simple_properties[tag]
                prop = Prop.from_dict(model_dir, val, label=key)

            elif tag in compound_properties:
                props = []
                weights = []

                for key2, val2 in val.items():
                    assert isinstance(val2, dict), errmsg.format('dict', val2)
                    _, tag2 = Property.get_label_and_tag_from_key(key2)
                    Prop = simple_properties[tag2]
                    prop = Prop.from_dict(model_dir, val2, label=key2)
                    props.append(prop)
                    weights.append(val2['weight'])

                Prop = compound_properties[tag]
                prop = Prop(props, weights, key)

            else:
                raise ValueError(f'Unknown property: {tag}')

            properties.append(prop)

        return cls(model_dir, properties)

    def get_model_name(self):
        model_name = os.path.basename(self.model_dir)
        return model_name

    def get_properties(self, sort=False):
        if sort:
            for key in self.sorted_keys:
                yield self[key]
        else:
            for prop in self.values():
                yield prop

    def get_unique_tags(self):
        tags = sorted(list(set([prop.tag for prop in self.get_properties()])))
        return tags

    def extract(self):
        for prop in self.get_properties():
            prop.extract()
        return

    def write(self, output_file=None):
        if output_file is None:
            output_file = f'results_{self.get_model_name()}.json'

        if os.path.exists(output_file):
            results = read_json(output_file, use_ase=True)
            action = 'Updating'
        else:
            results = dict()
            action = 'Writing to'

        print(f'{action} {output_file}')

        for prop in self.get_properties():
            value = prop.get_value()
            label = prop.get_label()
            results[label] = value

        write_json(results, output_file, use_ase=True)
        return
