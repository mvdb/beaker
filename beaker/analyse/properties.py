#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
import numpy as np
import os
import warnings
from ase import Atoms
from ase.build import minimize_rotation_and_translation
from ase.io import read


class Property:
    tag = None
    delimiter = '.'
    UNAVAILABLE = 'n/a'

    def __init__(self, label=None, value=None):
        self.label = label
        self.value = value

    def extract(self):
        if self.value is None:
            try:
                value = self._extract()
            except NotImplementedError:
                raise
            except Exception as err:
                label = self.get_label()
                warnings.warn(f'problem during {label} extraction ({err})')
                value = self.UNAVAILABLE

            self.value = value
        else:
            value = self.value
        return value

    def _extract(self):
        raise NotImplementedError('Must be implemented by child classes')

    def compare(self, prop):
        raise NotImplementedError('Must be implemented by child classes')

    def get_label(self):
        return self.label

    @staticmethod
    def get_label_and_tag_from_key(key):
        label, tag = key.rsplit(Property.delimiter, maxsplit=1)
        return (label, tag)

    def get_tag(self):
        return self.tag

    def get_value(self):
        return self.value

    def set_label(self, label):
        self.label = label
        return

    def set_value(self, value):
        self.value = value
        return


class ScalarProperty(Property):
    def compare(self, prop):
        val1 = self.get_value()
        val2 = prop.get_value()
        diff = val1 - val2
        return diff


class SimpleProperty(Property):
    def __init__(self, model_dir=None, struc_name=None, task=None,
                 label=None):
        self.model_dir = model_dir
        if model_dir is None:
            self.model_name = None
        else:
            self.model_name = os.path.basename(model_dir)
        self.struc_name = struc_name
        self.task = task

        if label is None:
            parts = [self.struc_name, self.task, self.tag]
            if all([part is not None for part in parts]):
                label = self.delimiter.join(parts)

        super().__init__(label=label)

    @classmethod
    def from_dict(cls, model_dir, dictionary, label=None):
        struc_name, task = dictionary['dir'].split('/')
        return cls(model_dir, struc_name, task, label=label)

    def get_workdir(self):
        parts = [self.model_dir, self.struc_name, self.task]
        workdir = os.path.join(*parts)
        return workdir

    def read_from_beaker_output(self, pattern, convert=str):
        workdir = self.get_workdir()
        filename = os.path.join(workdir, 'beaker.out')

        with open(filename, 'r') as f:
            for line in f:
                if line.startswith(pattern):
                    value = convert(line.split()[-1])
                    break
            else:
                raise ValueError(f'Could not find {pattern} in {filename}')
        return value


class ArrayProperty:
    def compare(self, prop):
        val1 = self.get_value()
        val2 = prop.get_value()
        diff = ArrayProperty.calculate_rmsd(val1, val2)
        return diff

    @staticmethod
    def calculate_rmsd(val1, val2):
        N = len(val1)
        assert np.shape(val1) == np.shape(val2), \
               'Arrays do not have the same shape'

        rmsd = 0.
        for i in range(N):
            rmsd += np.sum((val1[i] - val2[i])**2)
        rmsd = np.sqrt(rmsd / N)
        return rmsd


class StructureProperty(SimpleProperty):
    tag = None

    def _extract(self):
        workdir = self.get_workdir()
        filename = os.path.join(workdir, 'final_atoms.traj')
        value = read(filename).todict()
        for key in list(value.keys()):
            if key not in ['cell', 'numbers', 'positions', 'pbc']:
                del value[key]
        return value

    def get_atoms(self):
        atoms = None if self.value is None else Atoms(**self.value)
        return atoms


class BandGapProperty(SimpleProperty, ScalarProperty):
    tag = 'band_gap'

    def _extract(self):
        value = self.read_from_beaker_output('band_gap', convert=float)
        return value


class BulkModulusProperty(SimpleProperty, ScalarProperty):
    tag = 'bulk_mod'

    def _extract(self):
        value = self.read_from_beaker_output('final_B', float)
        return value


class CellAngProperty(StructureProperty):
    tag = 'cellang'

    def compare(self, prop):
        reference = self.get_atoms()
        atoms = prop.get_atoms()
        cellang_ref = np.array(reference.cell.cellpar())[3:]
        cellang_atoms = np.array(atoms.cell.cellpar())[3:]
        maxdev = np.max(np.abs(cellang_atoms - cellang_ref))
        return maxdev


class CellVecProperty(StructureProperty):
    tag = 'cellvec'

    def compare(self, prop):
        reference = self.get_atoms()
        atoms = prop.get_atoms()
        cellvec_ref = np.array(reference.cell.cellpar())[:3]
        cellvec_atoms = np.array(atoms.cell.cellpar())[:3]
        diff_percent = 100 - 100. * cellvec_atoms / cellvec_ref
        mad = np.average(np.abs(diff_percent))
        return mad


class CoordProperty(StructureProperty, ArrayProperty):
    tag = 'coord'

    def compare(self, prop):
        reference = self.get_atoms()
        atoms = prop.get_atoms()
        minimize_rotation_and_translation(reference, atoms)
        pos_atoms = atoms.get_positions()
        pos_ref = reference.get_positions()
        rmsd = ArrayProperty.calculate_rmsd(pos_atoms, pos_ref)
        return rmsd


class EnergyProperty(SimpleProperty, ScalarProperty):
    tag = 'energy'

    def _extract(self):
        value = self.read_from_beaker_output('final_energy', float)
        return value


class VibFreqProperty(SimpleProperty, ScalarProperty):
    tag = 'vibfreq'

    def __init__(self, mode=None, **kwargs):
        super().__init__(**kwargs)
        self.mode = mode

    @classmethod
    def from_dict(cls, model_dir, dictionary, label=None):
        struc_name, task = dictionary['dir'].split('/')
        mode = np.array(dictionary['mode'])
        return cls(mode=mode, model_dir=model_dir, struc_name=struc_name,
                   task=task, label=label)

    def find_matching_mode(self):
        workdir = self.get_workdir()
        filename = os.path.join(workdir, 'final_eigenmodes.npy')
        modes = np.load(filename)

        diffs = []
        for mode in modes:
            diff_para = np.linalg.norm(mode - self.mode)
            diff_anti = np.linalg.norm(mode + self.mode)
            diffs.append(min(diff_para, diff_anti))

        index = np.argmin(diffs)
        mode = modes[index, :, :]
        return (index, mode)

    def _extract(self):
        index, _ = self.find_matching_mode()
        workdir = self.get_workdir()
        filename = os.path.join(workdir, 'final_eigenvalues.txt')
        eigenvalues = np.loadtxt(filename, dtype=np.complex_)
        value = np.real(eigenvalues[index, 1])  # in cm^-1
        return value


class XCoordProperty(StructureProperty):
    tag = 'xcoord'

    @staticmethod
    def get_scaled_positions(atoms, rtol=1e-2):
        xpos = atoms.get_scaled_positions(wrap=True)
        for i in range(len(atoms)):
            for j in range(3):
                if np.isclose(xpos[i, j], 1, rtol=rtol):
                    xpos[i, j] -= 1
                elif np.isclose(xpos[i, j], -1, rtol=rtol):
                    xpos[i, j] += 1
        return xpos

    def compare(self, prop):
        reference = self.get_atoms()
        atoms = prop.get_atoms()
        xpos_ref = XCoordProperty.get_scaled_positions(reference)
        xpos_atoms = XCoordProperty.get_scaled_positions(atoms)
        maxdev = np.max(np.abs(xpos_atoms - xpos_ref)) * 100  # in %
        return maxdev


simple_properties = {
    propclass.tag: propclass
    for propclass in [
        BandGapProperty,
        BulkModulusProperty,
        CellAngProperty,
        CellVecProperty,
        CoordProperty,
        EnergyProperty,
        VibFreqProperty,
        XCoordProperty,
    ]
}


class CompoundProperty(Property):
    def __init__(self, properties=None, weights=None, label=None):
        self.properties = properties
        self.weights = weights
        super().__init__(label=label)

    def _extract(self):
        values = [prop.extract() for prop in self.properties]
        if any([value is None for value in values]):
            value = None
        elif any([value == Property.UNAVAILABLE for value in values]):
            value = Property.UNAVAILABLE
        else:
            for i, weight in enumerate(self.weights):
                values[i] *= weight
            value = sum(values)
        return value


class EdiffProperty(CompoundProperty, ScalarProperty):
    tag = 'ediff'


compound_properties = {
    propclass.tag: propclass
    for propclass in [EdiffProperty]
}
