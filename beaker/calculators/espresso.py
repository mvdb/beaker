#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
from ase.calculators.espresso import Espresso as EspressoBase
from beaker.utils.io import prepend_initial_working_directory


class Espresso(EspressoBase):
    supports_socket_io = True

    def __init__(self, atoms, unixsocket=None, kpts=None, spinpol=False,
                 verbose=False, model_name=None, **model_kwargs):

        nspin = 2 if spinpol else 1

        espresso_kwargs = model_kwargs.copy()

        for key in ['pseudo_dir']:
            dir = espresso_kwargs.get(key, None)
            if dir is not None:
                dir = prepend_initial_working_directory(dir)
                espresso_kwargs.update(pseudo_dir=dir)

        verbosity = 'high' if verbose else 'low'
        espresso_kwargs.update(verbosity=verbosity)

        espresso_kwargs.update(tprnfor=True, tstress=any(atoms.get_pbc()))

        command = espresso_kwargs.get('command', None)
        if command is None:
            if unixsocket is None:
                extra_cmd_opt = ''
            else:
                extra_cmd_opt = '--ipi {unixsocket}:UNIX'
            command = f'pw.x < PREFIX.pwi {extra_cmd_opt} ' + \
                      '1>PREFIX.pwo 2>PREFIX.pwe'

        if unixsocket is not None:
            command.format(unixsocket=unixsocket)
        espresso_kwargs.update(command=command)

        EspressoBase.__init__(self, nspin=nspin, kpts=kpts, **espresso_kwargs)
        self.model_name = model_name
        self.unixsocket = unixsocket

    def setup_dos_run(self):
        self.set(verbosity='high')
        return

    def setup_restart_run(self):
        raise NotImplementedError

    def setup_bandstructure_run(self):
        raise NotImplementedError

    def get_number_of_spins(self):
        nspin = self.calc.get_number_of_spins()
        return nspin

    def get_eigenvalues(self, kpt=0, spin=0):
        return self.calc.get_eigenvalues(kpt=kpt, spin=spin)

    def get_occupation_numbers(self, kpt=0, spin=0):
        raise NotImplementedError

    def get_magnetization(self):
        nspin = self.get_number_of_spins()

        if nspin == 1:
            magnetization = 0.
        else:
            magmom = self.get_magnetic_moments()
            magnetization = sum(magmom)

        return magnetization
