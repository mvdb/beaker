#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
import warnings
from ase.io import read, write
from ase.calculators.calculator import (FileIOCalculator, PropertyNotPresent)


error_template = 'Property "%s" not available. Please try running GPAW ' \
                 'first by calling e.g. atoms.get_potential_energy().'


warn_template = 'Property "%s" is None. Typically, this is because the ' \
                'required information has not been calculated by GPAW.'


script_template = """import numpy as np
from ase.calculators.socketio import SocketClient
from ase.io import read
from gpaw import GPAW, FermiDirac, Mixer, MixerDif, MixerSum, PW

atomsfile = '{prefix}.traj'
fixed_density = {fixed_density}
gpwfile = '{prefix}.gpw'
kwargs = dict(
    {kwargstr}
)
unixsocket = '{unixsocket}'
use_stress = {use_stress}

atoms = read(atomsfile)

if fixed_density:
    calc_prerun = GPAW(gpwfile, txt=None)
    nelect = int(calc_prerun.get_number_of_electrons())

    if kwargs['mode'] == 'lcao':
        eigensolver = None
        nbands = calc_prerun.get_number_of_bands()
    else:
        eigensolver = 'cg'
        nbands = 2 * nelect

    atoms.calc = calc_prerun.fixed_density(
                    convergence=dict(bands=nelect),
                    eigensolver=eigensolver,
                    kpts=kwargs['kpts'],
                    nbands=nbands,
                    symmetry='off',
                    txt=kwargs['txt'],
                 )
else:
    atoms.calc = GPAW(**kwargs)

if unixsocket == 'None':
    atoms.get_potential_energy()
    atoms.get_forces()
    if use_stress:
        atoms.get_stress()
else:
    client = SocketClient(unixsocket=unixsocket)
    for i, _ in enumerate(client.irun(atoms, use_stress=use_stress)):
        print('step:', i)

if not use_stress:
    atoms.calc.results['stress'] = None

atoms.calc.results['magmom'] = atoms.get_magnetic_moment()
atoms.calc.write(gpwfile)
"""


class GPAWBase(FileIOCalculator):
    command = 'python PREFIX.py PREFIX.traj > PREFIX.out'

    def __init__(self, atoms, restart=None, label='gpawbase', kpts=None,
                 spinpol=False, unixsocket=None, input_kwargs={}, **kwargs):
        FileIOCalculator.__init__(self, atoms=atoms, restart=restart,
                                  label=label, **kwargs)
        self.kpts = kpts
        self.spinpol = spinpol
        self.unixsocket = unixsocket
        self.input_kwargs = input_kwargs

        self.implemented_properties = ['energy', 'forces']
        if self.get_mode().startswith('PW'):
            self.implemented_properties.append('stress')

        self.calc = None
        self.fixed_density = False

    def get_mode(self):
        try:
            mode = self.input_kwargs['mode']
            mode = mode.strip('\'\"')
        except KeyError:
            mode = 'fd'
        return mode

    def set(self, **kwargs):
        changed_parameters = FileIOCalculator.set(self, **kwargs)
        if changed_parameters:
            self.reset()

    def write_input(self, atoms, properties=None, system_changes=None):
        FileIOCalculator.write_input(self, atoms, properties, system_changes)

        kwargs = [
            f'kpts={self.kpts}',
            f'spinpol={self.spinpol}',
            f'txt="{self.prefix}.txt"',
        ]
        for key, val in self.input_kwargs.items():
            kwarg = f'{key}={val}'
            kwargs.append(kwarg)

        kwargstr = ',\n    '.join(kwargs)

        use_stress = 'stress' in self.implemented_properties

        script_kwargs = dict(
            fixed_density=self.fixed_density,
            kwargstr=kwargstr,
            prefix=self.prefix,
            unixsocket=self.unixsocket,
            use_stress=use_stress,
        )

        scriptname = f'{self.prefix}.py'
        with open(scriptname, 'w') as f:
            lines = script_template.format(**script_kwargs)
            f.write(lines)

        strucname = f'{self.prefix}.traj'
        write(strucname, atoms)
        return

    def read_results(self):
        atoms = read(f'{self.prefix}.gpw')

        self.results['energy'] = atoms.get_potential_energy(
                                           force_consistent=True)
        self.results['forces'] = atoms.get_forces()

        if 'stress' in self.implemented_properties:
            self.results['stress'] = atoms.get_stress()
        else:
            self.results['stress'] = None

        self.calc = atoms.calc
        return

    def get_eigenvalues(self, **kwargs):
        if self.calc is None:
            raise PropertyNotPresent(error_template % 'Eigenvalues')
        eigenvalues = self.calc.get_eigenvalues(**kwargs)
        if eigenvalues is None:
            warnings.warn(warn_template % 'Eigenvalues')
        return eigenvalues

    def get_fermi_level(self):
        if self.calc is None:
            raise PropertyNotPresent(error_template % 'Fermi level')
        return self.calc.get_fermi_level()

    def get_ibz_k_points(self):
        if self.calc is None:
            raise PropertyNotPresent(error_template % 'IBZ k-points')
        ibzkpts = self.calc.get_ibz_k_points()
        if ibzkpts is None:
            warnings.warn(warn_template % 'IBZ k-points')
        return ibzkpts

    def get_k_point_weights(self):
        if self.calc is None:
            raise PropertyNotPresent(error_template % 'K-point weights')
        k_point_weights = self.calc.get_k_point_weights()
        if k_point_weights is None:
            warnings.warn(warn_template % 'K-point weights')
        return k_point_weights

    def get_magnetization(self):
        if self.calc is None:
            raise PropertyNotPresent(error_template % 'Magnetization')
        return self.calc.get_magnetic_moment()

    def get_number_of_spins(self):
        if self.calc is None:
            raise PropertyNotPresent(error_template % 'Number of spins')
        nspins = self.calc.get_number_of_spins()
        if nspins is None:
            warnings.warn(warn_template % 'Number of spins')
        return nspins

    def get_occupation_numbers(self, **kwargs):
        if self.calc is None:
            raise PropertyNotPresent(error_template % 'Occupations')
        occupations = self.calc.get_occupation_numbers(**kwargs)
        if occupations is None:
            warnings.warn(warn_template % 'Occupations')

        nspins = self.get_number_of_spins()
        if nspins == 1:
            occupations *= 2
        return occupations


class GPAW(GPAWBase):
    supports_socket_io = True

    def __init__(self, atoms, unixsocket=None, kpts=None, spinpol=False,
                 verbose=False, model_name=None, **model_kwargs):
        gpaw_kwargs = dict(
            kpts=kpts,
            spinpol=spinpol,
            unixsocket=unixsocket,
            input_kwargs=model_kwargs,
        )
        GPAWBase.__init__(self, atoms, **gpaw_kwargs)

        self.model_name = model_name
        self.unixsocket = unixsocket

    def setup_dos_run(self):
        return

    def setup_restart_run(self):
        return

    def setup_bandstructure_run(self, atoms, bandpath, npoints):
        self.fixed_density = True
        self.kpts = dict(path=bandpath, npoints=npoints)
        self.reset()
        return
