#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
from beaker.calculators import all_calculators
from beaker.utils.random import generate_random_string


def get_calculator(atoms, model_name, model_kwargs, struc_kwargs,
                   use_socket_io=True, verbose=False):
    delimiter = '_'
    assert not model_name.startswith(delimiter), \
           f'Model names should not start with "{delimiter}"'

    calc_name = model_name
    if delimiter in model_name:
        calc_name, _ = model_name.split(delimiter, maxsplit=1)

    for Calc in all_calculators:
        if Calc.__name__.lower() == calc_name.lower():
            break
    else:
        raise NotImplementedError(f'Unknown calculator: {calc_name}')

    calc_kwargs = dict(kpts=None, spinpol=False)

    for key in ['kpts', 'spinpol']:
        if key in struc_kwargs:
            calc_kwargs[key] = struc_kwargs[key]

    calc_kwargs.update(**model_kwargs)
    calc_kwargs.update(model_name=model_name, verbose=verbose)

    if use_socket_io and Calc.supports_socket_io:
        rnd = generate_random_string(length=6)
        unixsocket = f'beaker_{rnd}'
        calc_kwargs.update(unixsocket=unixsocket)

    calc = Calc(atoms, **calc_kwargs)
    return calc
