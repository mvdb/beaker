#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
from beaker.calculators.emt import EMT
from beaker.calculators.espresso import Espresso
try:
    from beaker.calculators.gpaw import GPAW
except ImportError:
    GPAW = None
from beaker.calculators.siesta import Siesta
try:
    from beaker.calculators.tibi import Tibi
except ImportError:
    Tibi = None


_Calcs = [
    EMT,
    Espresso,
    GPAW,
    Siesta,
    Tibi,
]

all_calculators = [Calc for Calc in _Calcs if Calc is not None]
