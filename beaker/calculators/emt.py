#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
from ase.io import ulm, write
from ase.calculators.calculator import FileIOCalculator


script_template = """
from ase.calculators.emt import EMT, parameters
from ase.calculators.socketio import SocketClient
from ase.io import read, ulm

atomsfile = '{prefix}.traj'
ulmfile = '{prefix}.ulm'
kwargs = dict(
    {kwargstr}
)
unixsocket = '{unixsocket}'

parameters.update(Si=parameters['Cu'])

atoms = read(atomsfile)
atoms.calc = EMT(**kwargs)

use_stress = {use_stress}

if unixsocket != 'None':
    client = SocketClient(unixsocket=unixsocket)
    for i, _ in enumerate(client.irun(atoms, use_stress=use_stress)):
        print('step:', i)

energy = atoms.get_potential_energy()
forces = atoms.get_forces()
stress = atoms.get_stress() if use_stress else None

with ulm.open(ulmfile, 'w') as f:
    f.write(energy=energy, forces=forces, stress=stress)
"""


class EMTBase(FileIOCalculator):
    command = 'python PREFIX.py PREFIX.traj > PREFIX.out'

    def __init__(self, atoms, restart=None, label='emtbase', kpts=None,
                 spinpol=False, unixsocket=None, input_kwargs={}, **kwargs):
        FileIOCalculator.__init__(self, atoms=atoms, restart=restart,
                                  label=label, **kwargs)
        self.kpts = kpts
        self.spinpol = spinpol
        self.unixsocket = unixsocket
        self.input_kwargs = input_kwargs

        self.implemented_properties = ['energy', 'forces']
        if all(atoms.pbc):
            self.implemented_properties.append('stress')

    def write_input(self, atoms, properties=None, system_changes=None):
        FileIOCalculator.write_input(self, atoms, properties, system_changes)

        kwargs = []
        for key, val in self.input_kwargs.items():
            kwarg = f'{key}={val}'
            kwargs.append(kwarg)
        kwargstr = ',\n    '.join(kwargs)

        use_stress = 'stress' in self.implemented_properties

        script_kwargs = dict(
            kwargstr=kwargstr,
            prefix=self.prefix,
            unixsocket=self.unixsocket,
            use_stress=use_stress,
        )

        scriptname = f'{self.prefix}.py'
        with open(scriptname, 'w') as f:
            lines = script_template.format(**script_kwargs)
            f.write(lines)

        strucname = f'{self.prefix}.traj'
        write(strucname, atoms)
        return

    def read_results(self):
        self.results = ulm.open(f'{self.prefix}.ulm')

    def get_number_of_spins(self):
        return 1

    def get_magnetization(self):
        return 0


class EMT(EMTBase):
    supports_socket_io = True

    def __init__(self, atoms, unixsocket=None, kpts=None, spinpol=False,
                 verbose=False, model_name=None, **model_kwargs):
        emt_kwargs = dict(
            kpts=kpts,
            spinpol=spinpol,
            unixsocket=unixsocket,
            input_kwargs=model_kwargs,
        )
        EMTBase.__init__(self, atoms, **emt_kwargs)

        self.model_name = model_name
        self.unixsocket = unixsocket

    def setup_dos_run(self):
        raise NotImplementedError

    def setup_bandstructure_run(self, atoms, bandpath, npoints):
        raise NotImplementedError
