#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
import numpy as np
from ase.calculators.calculator import kpts2ndarray
from beaker.utils.io import prepend_initial_working_directory
from tibi import Tibi as TibiBase


class Tibi(TibiBase):
    supports_socket_io = True

    def __init__(self, atoms, unixsocket=None, kpts=None, spinpol=False,
                 verbose=False, model_name=None, **model_kwargs):
        magmoms = atoms.get_initial_magnetic_moments()
        symbols = atoms.get_chemical_symbols()
        symbols_unique = list(set(symbols))

        tibi_kwargs = dict(
            complex_wfn=self.is_periodic(atoms) and kpts is not None,
            kpts=(1, 1, 1) if kpts is None else kpts,
            socket_type='unix',
            socket_unix_suffix=unixsocket,
            spin_nupdown=sum(magmoms),
            spin_polarized=spinpol,
            task='energy' if unixsocket is None else 'socket',
        )

        directory_keys = [
            'tbpar_dir',
        ]

        symbol_dependent_keys = [
            'aux_basis_giese_york',
            'basis_set',
            'scf_wkernel_on1c',
        ]

        for key, val in model_kwargs.items():
            if key in symbol_dependent_keys:
                v = {sym: val[sym] for sym in symbols_unique}
            elif key in directory_keys and val is not None:
                v = prepend_initial_working_directory(val)
            elif val == 'VERBOSE':
                v = verbose
            else:
                v = val
            tibi_kwargs[key] = v

        TibiBase.__init__(self, **tibi_kwargs)
        self.model_name = model_name
        self.unixsocket = unixsocket

        self.write_scf_restart_file(symbols, magmoms, spinpol, **model_kwargs)

    def setup_dos_run(self):
        self.set(
            output_bands=True,
        )
        return

    def setup_restart_run(self):
        self.set(
            scf_restart_output_file='scf_restart.out',
            scf_write_restart_file=True,
        )
        return

    def setup_bandstructure_run(self, atoms, bandpath, npoints):
        kpts_file = 'kpoints.in'
        self.write_kpoints_file(atoms, bandpath, npoints, filename=kpts_file)

        with open('scf_restart.out', 'r') as f:
            lines = f.readlines()

        with open('scf_restart.in', 'w') as f:
            for line in lines:
                f.write(line)

        self.set(
            kpts=kpts_file,
            output_bands=True,
            scf_initial_guess='restart',
            scf_maxiter=1,
            scf_must_converge=False,
            scf_restart_input_file='scf_restart.in',
            scf_write_restart_file=False,
        )
        return

    def write_kpoints_file(self, atoms, bandpath, npoints,
                           filename='kpoints.in'):
        kpts = dict(path=bandpath, npoints=npoints)
        kpts_coord = kpts2ndarray(kpts, atoms=atoms)
        weight = [1./len(kpts_coord)]

        with open(filename, 'w') as f:
            for coord in kpts_coord:
                print(*coord, *weight, file=f)
        return

    @staticmethod
    def is_periodic(atoms):
        pbc = atoms.get_pbc()
        if all(pbc):
            periodic = True
        elif any(pbc):
            raise NotImplementedError('Mixed PBCs are not supported')
        else:
            periodic = False
        return periodic

    @staticmethod
    def write_scf_restart_file(symbols, magmoms, spinpol, **kwargs):
        filename = 'scf_restart.in'

        with open(filename, 'w') as f:
            for symbol, magmom in zip(symbols, magmoms):
                chgmag = Tibi.get_atom_initial_chgmag(symbol, magmom,
                                                      spinpol, **kwargs)
                f.write(' '.join(map(lambda x: '%.2f' % x, chgmag)))
                f.write('\n')
        return

    @staticmethod
    def get_atom_initial_chgmag(symbol, magmom, spinpol, **kwargs):
        try:
            mapping = kwargs['scf_aux_mapping']
        except KeyError:
            mapping = 'mulliken'

        if mapping == 'giese_york':
            auxbas = kwargs['aux_basis_giese_york'][symbol]

            nzeta = int(auxbas[0])
            lmax = 'SPDF'.index(auxbas[1])
            nmulti = (lmax + 1)**2
            chgmag = [0.] * nzeta * nmulti
            if spinpol:
                m = magmom / np.sqrt(4 * np.pi)
                chgmag += [m] + [0.] * (nzeta * nmulti - 1)

        elif mapping == 'mulliken':
            mainbas = kwargs['basis_set'][symbol]
            try:
                auxbas = kwargs['aux_basis_mulliken']
            except KeyError:
                auxbas = 'atom_dependent'

            if auxbas == 'atom_dependent':
                chgmag = [0., magmom] if spinpol else [0.]
            elif auxbas == 'subshell_dependent':
                nsubshell = len(mainbas) - mainbas.count('_')
                chgmag = [0.] * nsubshell
                if spinpol:
                    if mainbas == 'sp':
                        chgmag += [magmom, 0.]
                    elif mainbas == 'sd':
                        chgmag += [0., magmom]
                    elif mainbas == 'spd':
                        chgmag += [magmom/2., 0., magmom/2.]
                    elif mainbas == 'sp_s':
                        chgmag += [magmom, 0., 0.]
                    elif mainbas == 'spd_sp':
                        chgmag += [magmom/2., magmom/2., 0., 0., 0.]
                    elif mainbas == 'spd_sd':
                        chgmag += [0., 0., magmom, 0., 0.]
                    else:
                        raise NotImplementedError(mainbas)
            else:
                raise NotImplementedError(auxbas)
        else:
            raise NotImplementedError(mapping)

        return chgmag
