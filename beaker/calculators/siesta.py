#-----------------------------------------------------------------------------#
#   Beaker: a simple tool for managing atomic simulation tasks                #
#   Copyright 2023-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#
import numpy as np
import os
from shutil import copyfile
from ase.calculators.siesta import Siesta as SiestaBase
from beaker.utils.io import prepend_initial_working_directory


class Siesta(SiestaBase):
    supports_socket_io = True

    def __init__(self, atoms, unixsocket=None, kpts=None, spinpol=False,
                 verbose=False, model_name=None, **model_kwargs):
        spin = 'collinear' if spinpol else 'non-polarized'

        siesta_kwargs = model_kwargs.copy()

        for key in ['pseudo_path']:
            dir = siesta_kwargs.get(key, None)
            if dir is not None:
                dir = prepend_initial_working_directory(dir)
                siesta_kwargs.update(pseudo_path=dir)

        if 'fdf_arguments' not in siesta_kwargs:
            siesta_kwargs['fdf_arguments'] = {}

        # Special treatment of User.Basis fdf argument
        try:
            ionfile_dirs = siesta_kwargs['fdf_arguments']['User.Basis']
            if isinstance(ionfile_dirs, str):
                ionfile_dirs = [ionfile_dirs]
            ionfile_dirs = [prepend_initial_working_directory(dir)
                            for dir in ionfile_dirs]
        except KeyError:
            ionfile_dirs = None

        if ionfile_dirs is not None and ionfile_dirs:
            siesta_kwargs['fdf_arguments']['User.Basis'] = 'True'
            xc = 'LDA' if 'xc' not in siesta_kwargs else siesta_kwargs['xc']
            self.copy_ion_files(atoms, xc, ionfile_dirs)

        if unixsocket is not None:
            siesta_kwargs['fdf_arguments'].update({
                'Master.address': unixsocket,
                'Master.code': 'i-pi',
                'Master.interface': 'socket',
                'Master.socketType': 'unix',
                'MD.TypeOfRun': 'Master',
            })

        SiestaBase.__init__(self, spin=spin, kpts=kpts, **siesta_kwargs)
        self.model_name = model_name
        self.unixsocket = unixsocket

    def setup_dos_run(self):
        fdf_arguments = self.parameters['fdf_arguments']
        if fdf_arguments is None:
            fdf_arguments = {}
        fdf_arguments.update(WriteEigenvalues=True)
        self.set(fdf_arguments=fdf_arguments)
        return

    def setup_restart_run(self):
        raise NotImplementedError

    def setup_bandstructure_run(self):
        raise NotImplementedError

    def get_number_of_spins(self):
        spin = self['spin']
        if spin == 'non-polarized':
            nspin = 1
        elif spin == 'collinear':
            nspin = 2
        else:
            raise NotImplementedError(spin)
        return nspin

    def get_eigenvalues(self, kpt=0, spin=0):
        return self.results['eigenvalues'][spin, kpt, :]

    def get_occupation_numbers(self, kpt=0, spin=0):
        eigenvalues = self.results['eigenvalues']
        e_fermi = self.get_fermi_level()
        nspin = self.get_number_of_spins()

        tol = 5e-2
        occupations = np.zeros_like(eigenvalues)
        selection = eigenvalues < (e_fermi - tol)
        occupations[selection] = nspin
        selection = ~selection * (eigenvalues < (e_fermi + tol))
        occupations[selection] = 0.5 * nspin
        return occupations[spin, kpt, :]

    def read_magnetization(self):
        fname = self.getpath(ext='out')
        with open(fname, 'r') as fd:
            text = fd.read().lower()

        magnetization = None

        lines = iter(text.split('\n'))
        for line in lines:
            if 'spin moment:' in line:
                words = line.split()
                magnetization = float(words[-4])

        assert magnetization is not None, \
               'Could not extract magnetization from Siesta output'

        self.results['magnetization'] = magnetization
        return

    def get_magnetization(self):
        nspin = self.get_number_of_spins()

        if nspin == 1:
            magnetization = 0.
        else:
            if 'magnetization' not in self.results:
                self.read_magnetization()
            magnetization = self.results['magnetization']

        return magnetization

    @staticmethod
    def copy_ion_files(atoms, xc, ionfile_dirs):
        xc_class = 'lda' if xc == 'LDA' else 'gga'

        counter = 1
        done = []

        for atom in atoms:
            sym = atom.symbol

            if sym not in done:
                ionfile = f'{sym}.{xc_class}.ion'

                for ionfile_dir in ionfile_dirs:
                    src = os.path.join(ionfile_dir, ionfile)
                    if os.path.exists(src):
                        break
                else:
                    msg = f'{ionfile} file not found among {ionfile_dirs}'
                    raise OSError(msg)

                dest = f'{sym}.{xc_class}.{counter}.ion'
                copyfile(src, dest)

                done.append(sym)
                counter += 1
        return
