import re
from pathlib import Path
from setuptools import find_packages, setup

# Get the version number:
with open('beaker/__init__.py') as f:
    version = re.search("__version__ = '(.*)'", f.read()).group(1)

files = [
    'beaker-compare',
    'beaker-extract',
    'beaker-run',
]

scripts = [str(Path('tools') / f) for f in files]

install_requires = [
    'ase>=3.22.0',
    'matplotlib',
    'numpy',
    'pytest',
    'pytest-env',
    'pyaml-env',
    'pyyaml',
]

setup(
    install_requires=install_requires,
    license='LICENSE',
    name='beaker',
    packages=find_packages(),
    python_requires='>=3.6',
    scripts=scripts,
    url='https://gitlab.com/mvdb/beaker',
    version=version,
)
