import os
import pytest
import sys
from ase.io import read, write
from beaker.cli.run import main as main_run
from setup_cli_run import main as main_setup


EMT = 'emt'
ESPRESSO = 'espresso'
GPAW_LCAO = 'gpaw_lcao'
GPAW_PW = 'gpaw_pw'
SIESTA = 'siesta'
TIBI = 'tibi'

COMMON_TASKS = ['relax', 'relax_eos', 'vib']

TESTSUITE_TASKS = {
    EMT: COMMON_TASKS + ['dimer', 'eos', 'relax_vc'],
    # skipping dimer tasks for Espresso model (no negative curvature)
    ESPRESSO: COMMON_TASKS + ['eos', 'relax_vc'],
    GPAW_LCAO: COMMON_TASKS + ['dimer', 'dos'],
    GPAW_PW: COMMON_TASKS + ['bands', 'dimer', 'dos', 'eos', 'relax_vc'],
    SIESTA: COMMON_TASKS + ['dimer', 'dos', 'eos', 'relax_vc'],
    TIBI: COMMON_TASKS + ['bands', 'dimer', 'dos', 'eos', 'relax_vc'],
}


@pytest.fixture(scope='session', autouse=True)
def setup():
    main_setup()
    return


def main_run_wrapper():
    exitcode = main_run()
    assert exitcode == 0, 'beaker.cli.run.main() exited with nonzero exit code'
    return


@pytest.mark.parametrize('model_task',
                         ['-'.join([model, task])
                          for model, tasks in TESTSUITE_TASKS.items()
                          for task in tasks])
def test_cli_run(model_task):
    model, task = model_task.split('-', maxsplit=1)

    arg0 = sys.argv[0]
    sys.argv = [
        arg0,
        '-r', './runs.yaml',
        '-m', f'../models/{model}.yaml',
        '-t', f'^{task}$',
        '--redo',
    ]

    main_run_wrapper()
    return


def test_multiple_models_tasks_structures_processes():
    model1 = EMT
    model2 = TIBI

    arg0 = sys.argv[0]
    sys.argv = [
        arg0,
        '-r', './runs.yaml',
        '-m', f'../models/{model1}.yaml,../models/{model1},../models/{model2}',
        '-t', 'relax|vib',
        '-p', '2',
        '--redo',
    ]

    main_run_wrapper()
    return


@pytest.mark.parametrize('model', [GPAW_LCAO])
def test_bands_gpaw_lcao(model):
    """
    One way to currently run a 'bands' task with GPAW in LCAO mode
    (where the prior 'relax_vc' task cannot be performed)
    is to just provide a custom 'relax_vc/final_atoms.traj' file.
    A bit hacky, but worth testing.
    """
    struc = 'si_diamond'
    task = 'bands'

    relax_vc_dir = os.path.join(model, struc, 'relax_vc')
    os.makedirs(relax_vc_dir, exist_ok=True)

    source = os.path.join('structures', 'start_si_diamond.xyz')
    destination = os.path.join(relax_vc_dir, 'final_atoms.traj')
    atoms = read(source)
    write(destination, atoms)

    arg0 = sys.argv[0]
    sys.argv = [
        arg0,
        '-m', f'../models/{model}.yaml',
        '-i', f'^{struc}$',
        '-t', f'^{task}$',
        '--redo',
    ]

    main_run_wrapper()
    return
