import numpy as np
import os
import yaml
from ase import Atoms
from ase.build import bulk, molecule
from ase.io import write


STRUC_NAMES = [
    'si_diamond',
    'si_dimer',
    'si_trimer',
]

STRUC_DIR = 'structures'


def generate_si_diamond(struc_name):
    atoms = bulk('Si', crystalstructure='diamond')
    filename = os.path.join(STRUC_DIR, f'start_{struc_name}.xyz')
    write(filename, atoms)

    run = dict(
        bands_info=dict(
            bandpath='WGXWLG',
            is_metallic=False,
            npoints=50,
            ),
        dos_info=dict(
            is_metallic=False,
        ),
        filename=filename,
        kpts=[3, 3, 3],
        spinpol=False,
        tasks=['bands', 'dos', 'eos', 'relax', 'relax_eos', 'relax_vc'],
    )

    custom = [
        dict(
            structures=[struc_name],
            models='.*',
            tasks='^relax$|^relax_vc$',
            task_kwargs=dict(
                opt_runkwargs=dict(
                    fmax=0.5,
                ),
             ),
        ),
    ]
    return (run, custom)


def generate_si_dimer(struc_name):
    atoms = molecule('Si2')
    atoms.center(vacuum=4)

    filename = os.path.join(STRUC_DIR, f'start_{struc_name}.xyz')
    write(filename, atoms)

    run = dict(
        dos_info=dict(is_metallic=True),
        filename=filename,
        kpts=None,
        spinpol=True,
        tasks=['dos', 'relax', 'vib'],
    )

    custom = [
        dict(
            structures=[struc_name],
            models='.*',
            tasks=['relax'],
            task_kwargs=dict(
                opt_runkwargs=dict(fmax=0.5),
            ),
        ),
        dict(
            structures=[struc_name],
            models='gpaw.*',
            tasks=['vib'],
            model_kwargs=dict(
                symmetry=dict(
                    point_group=False,
                ),
            ),
        ),
        dict(
            structures=[struc_name],
            models='gpaw.*',
            tasks='.*',
            model_kwargs=dict(
                occupations='FermiDirac(0.01)',
            ),
        ),
    ]
    return (run, custom)


def generate_si_trimer(struc_name):
    filename = os.path.join(STRUC_DIR, f'start_{struc_name}.xyz')
    positions = np.array([
        [0.]*3,
        [1.3]*3,
        [0., 3., 0.6],
    ])
    atoms = Atoms('Si3', positions=positions, pbc=False)
    atoms.center(vacuum=4)
    write(filename, atoms)

    startmode_filename = os.path.join(STRUC_DIR, f'mode_{struc_name}.txt')
    startmode = np.array([
        [0.]*3,
        [0.]*3,
        [1., 0., 1.],
    ])
    np.savetxt(startmode_filename, startmode)

    run = dict(
        dimer_info=dict(
            startmode_filename=startmode_filename,
        ),
        filename=filename,
        kpts=None,
        spinpol=False,
        tasks=['dimer'],
    )

    custom = [
        dict(
            structures=[struc_name],
            models='.*',
            tasks='dimer',
            task_kwargs=dict(
                dimopt_runkwargs=dict(
                    fmax=5.,
                ),
            ),
        ),
    ]
    return (run, custom)


def main():
    test_dir = os.path.dirname(__file__)
    assert os.path.exists(test_dir)
    os.chdir(test_dir)

    if not os.path.exists(STRUC_DIR):
        os.mkdir(STRUC_DIR)

    runs = dict()
    customs = []

    for struc_name in STRUC_NAMES:
        func_name = f'generate_{struc_name}'
        func = globals()[func_name]
        run, custom = func(struc_name)
        runs[struc_name] = run
        customs.extend(custom)

    with open('runs.yaml', 'w') as f:
        yaml.dump(runs, f)

    with open('custom.yaml', 'w') as f:
        yaml.dump(customs, f)
    return


if __name__ == '__main__':
    main()
