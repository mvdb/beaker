import os
import yaml


RUN_TEST_DIR = '../test_cli_run'


def generate_simple_properties(run_dict):
    properties = dict()

    mapping = {
        'bands': ['band_gap'],
        'dimer': ['coord', 'energy'],
        'dos': ['band_gap'],
        'eos': ['bulk_mod'],
        'relax': ['coord', 'energy'],
        'relax_eos': ['cellang', 'cellvec', 'coord', 'energy', 'xcoord'],
        'relax_vc': ['cellang', 'cellvec', 'coord', 'energy', 'xcoord'],
        'vib': ['vibfreq'],
    }

    for struc_name, options in run_dict.items():
        for task in options['tasks']:
            d = f'{struc_name}/{task}'

            for prop in mapping[task]:
                name = f'{task}.{struc_name}.{prop}'
                properties[name] = dict(dir=d)

                if prop == 'vibfreq':
                    properties[name].update(mode=[[0, 0, 1], [0, 0, -1]])

    return properties


def generate_compound_properties(run_dict):
    properties = {
        'si2_condensation.ediff': {
            'some_reactant.energy': {
                'dir': 'si_dimer/relax',
                'weight': -0.5,
            },
            'some_product.energy': {
                'dir': 'si_diamond/relax',
                'weight': 0.5,
            },
        },
    }
    return properties


def main():
    test_dir = os.path.dirname(__file__)
    assert os.path.exists(test_dir)
    os.chdir(test_dir)

    assert os.path.exists(RUN_TEST_DIR)

    run_file = os.path.join(RUN_TEST_DIR, 'runs.yaml')
    with open(run_file, 'r') as f:
        run_dict = yaml.safe_load(f)

    properties = generate_simple_properties(run_dict)
    properties.update(generate_compound_properties(run_dict))

    with open('properties.yaml', 'w') as f:
        yaml.dump(properties, f)
    return


if __name__ == '__main__':
    main()
