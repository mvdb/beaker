import pytest
import sys
from beaker.cli.compare import main as main_compare
from beaker.cli.extract import main as main_extract
from beaker.tasks import all_tasks
from setup_cli_extract import main as main_setup
from test_cli_run import GPAW_PW, TESTSUITE_TASKS, TIBI


@pytest.fixture(scope='session', autouse=True)
def setup():
    main_setup()
    return


@pytest.mark.parametrize('model', list(TESTSUITE_TASKS.keys()))
def test_cli_extract(model):
    arg0 = sys.argv[0]
    sys.argv = [
        arg0,
        '-m', f'../test_cli_run/{model}',
        '-p', 'properties.yaml',
    ]

    exclude = []
    for task in all_tasks:
        if task not in TESTSUITE_TASKS[model]:
            exclude.append(f'^{task}\\..*')
    exclude = '|'.join(exclude)

    if len(exclude) > 0:
        sys.argv += ['-x', exclude]

    print(f'\nOptions for {model}: {" ".join(sys.argv[1:])}')
    main_extract()
    return


def test_cli_extract_multiple_models_outputs():
    models = [GPAW_PW, TIBI]

    arg0 = sys.argv[0]
    sys.argv = [
        arg0,
        '-m', ','.join([f'../test_cli_run/{model}' for model in models]),
        '-o', ','.join([f'./out_{model}.json' for model in models]),
    ]

    main_extract()
    return


def test_cli_compare():
    results = [f'results_{model}.json' for model in [TIBI]]
    reference = f'results_{GPAW_PW}.json'

    sys.argv = [sys.argv[0]] + results + ['-r', reference]

    main_compare()
    return
